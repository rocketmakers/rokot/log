import * as Logger from "bunyan";
export {Logger}
export {ILoggerFactory,ILoggerSettings,getLoggerOptions,makeLogger,getSettingsLevel,LoggerLevel} from "./core";
export {ConsoleLogger,IConsoleLoggerSettings,getConsoleStream} from "./consoleLogger";
export {LogstashAmqpLogger,ILogstashAmqpLoggerSettings,getLogstashAmqpStream} from "./logstashAmqpLogger";
